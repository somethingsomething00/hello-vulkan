.PHONY: debug clean

LIB = -lm -lglfw -lvulkan

CC = gcc

CFLAGS =

FS_SRC = shaders/tri.frag
FS_OUT = shaders/bin/tri.frag.spv

VS_SRC = shaders/tri.vert
VS_OUT = shaders/bin/tri.vert.spv

BIN = vulkan-demo
SRC = vulkan-demo.c

# Uncomment to include shader compilation
# all: $(BIN) $(VS_OUT) $(FS_OUT)

all: $(BIN)




$(BIN): $(SRC)
	$(CC) $< -o $@ $(LIB) $(CFLAGS)


$(VS_OUT): $(VS_SRC)
	glslc $< -o $@

$(FS_OUT): $(FS_SRC)
	glslc $< -o $@


debug:
	@touch $(SRC)
	@make CFLAGS+=-g3

clean:
	rm -f $(BIN)
