/***********************************************************
* Hello triangle example for Vulkan written in C
* CREDITS TO:
* https://vulkan-tutorial.com
* Without this resource, I don't know how anyone could have
* intuitively figured out how to initialize Vulkan

* Push constants:
* https://vkguide.dev/docs/chapter-3/push_constants/

***********************************************************/

#include "all.h"

#define ArrayCount(a) (sizeof(a) / sizeof(a)[0])

#define VK_Assert(cond, ...) \
	do { \
		if(!(cond)) { \
			fprintf(stderr, "%s:%d ", __FILE__, __LINE__); \
			fprintf(stderr, "Assertion failed for expression '%s': ", #cond); \
			fprintf(stderr, __VA_ARGS__); \
			fprintf(stderr, "\n"); \
			exit(1); \
		} \
	} \
	while(0)


#define Alloc(thing, count) thing = calloc((count), sizeof *(thing))
#define Free(thing) free(thing)


#define VK_CHECK_RESULT(f) assert((f) == VK_SUCCESS)

// Avoids some allocations by having a fixed size array
#define ARRAY_SIZE 256

#define PI 3.141592654

// 2*2 2D rotation matrix
#define MATRIX_SIZE (2 * 2)

bool Running = true;

/**********************************
* GLFW specific
**********************************/
void CbWindowClose(GLFWwindow *Window)
{
	Running = false;
}

void CbKeys(GLFWwindow *Window, int key, int scancode, int action, int mod)
{
	if(key == GLFW_KEY_Q && action == GLFW_PRESS)
	{
		Running = 0;
	}

	if(key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
	{
		Running = 0;
	}
}


/**********************************
* Filesystem
**********************************/
typedef struct
{
	unsigned char *data;
	size_t size;
	int success;
} fs_file;

fs_file FS_ReadEntireFile(const char *path)
{
	fs_file File = {0};
	FILE *f = NULL;
	struct stat info;

	printf("FS_ReadEntireFile: Opening %s...\n", path);

	f = fopen(path, "rb");
	if(!f)
	{
		int ec = errno;
		char *err = strerror(ec);
		fprintf(stderr, "FS_ReadEntireFile: '%s': %s\n", path, err);
		return File;
	}
	if(fstat(fileno(f), &info) != 0) {fclose(f); return File;}
	File.size = info.st_size;
	File.data = malloc(File.size);
	assert(File.data != NULL);
	size_t bytesRead = fread(File.data, 1, File.size, f);
	assert(bytesRead == File.size);

	File.success = 1;
	fclose(f);

	return File;
}

void FS_FreeFile(fs_file *File)
{
	free(File->data);
	File->data = 0;
}


/**********************************
* Math
**********************************/
float Radians(float degrees)
{
	return degrees * (PI / 180.0);
}


/**********************************
* VK helpers
**********************************/
typedef struct
{
	VkSurfaceCapabilitiesKHR Caps;
	VkSurfaceFormatKHR *Formats;
	VkPresentModeKHR *PresentModes;

	u32 FormatCount;
	u32 PresentModeCount;

} vk_swap_chain_desc;

vk_swap_chain_desc VK_QuerySwapChainDesc(VkPhysicalDevice Device, VkSurfaceKHR Surface)
{
	vk_swap_chain_desc Desc = {0};

	vkGetPhysicalDeviceSurfaceCapabilitiesKHR(Device, Surface, &Desc.Caps);

	u32 FormatCount = 0;
	vkGetPhysicalDeviceSurfaceFormatsKHR(Device, Surface, &FormatCount,  0);
	if(FormatCount > 0)
	{
		Alloc(Desc.Formats, FormatCount);
		vkGetPhysicalDeviceSurfaceFormatsKHR(Device, Surface, &FormatCount,  Desc.Formats);
	}

	u32 PresentModeCount = 0;
	vkGetPhysicalDeviceSurfacePresentModesKHR(Device, Surface, &PresentModeCount, 0);
	if(PresentModeCount > 0)
	{
		Alloc(Desc.PresentModes, PresentModeCount);
		vkGetPhysicalDeviceSurfacePresentModesKHR(Device, Surface, &PresentModeCount, Desc.PresentModes);
	}

	Desc.FormatCount = FormatCount;
	Desc.PresentModeCount = PresentModeCount;

	VK_Assert(FormatCount > 0 && PresentModeCount > 0, "");

	return Desc;
}

void VK_FreeSwapChainDesc(vk_swap_chain_desc *Desc)
{
	Free(Desc->Formats);
	Free(Desc->PresentModes);
}

// @Incomplete
VkSurfaceFormatKHR VK_ChooseSwapSurfaceFormat(vk_swap_chain_desc *Desc)
{
	VkSurfaceFormatKHR SurfaceFormat = {0};

	// The tutorial wants us to use B8G8R8A8_SRGB but I'm fine with the default for now
	SurfaceFormat = Desc->Formats[0];


	return SurfaceFormat;
}

VkPresentModeKHR VK_ChooseSwapPresentMode(vk_swap_chain_desc *Desc)
{
	// According to the vulkan tutorial, a vulkan implmentation must support VK_PRESENT_MODE_FIFO_KHR
	// I tried using VKPRESENT_MODE_IMMEDIATE_KHR but the cpu usage was through the roof (no vsync)
	// and I don't know how to explicitly enable vsync in vulkan yet.

	// From testing, VK_PRESENT_MODE_FIFO_KHR seems to enable vsync by default

	VkPresentModeKHR PresentMode = VK_PRESENT_MODE_FIFO_KHR;
	VkPresentModeKHR Desired = VK_PRESENT_MODE_FIFO_KHR;
	for(int i = 0; i < Desc->PresentModeCount; i++)
	{
		if(Desc->PresentModes[i] == Desired)
		{
			PresentMode = Desired;
			break;
		}
	}

	return PresentMode;
}

// @Incomplete
VkExtent2D VK_ChooseSwapExtent(vk_swap_chain_desc *Desc)
{
	return Desc->Caps.currentExtent;
}

bool VK_CheckValidationLayerSupport(const char *DesiredLayer)
{
	bool found = false;

	u32 count = 0;
	vkEnumerateInstanceLayerProperties(&count, 0);
	assert(count < ARRAY_SIZE);

	VkLayerProperties Properties[ARRAY_SIZE] = {0};
	vkEnumerateInstanceLayerProperties(&count, Properties);

	for(int i = 0; i < count; i++)
	{
		const char *layer = Properties[i].layerName;
		if(strcmp(DesiredLayer, layer) == 0)
		{
			found = true;
			break;
		}
	}

	return found;
}

bool VK_CheckDeviceExtensionSupport(VkPhysicalDevice Device, const char *ExtensionString)
{
	bool supported = false;
	u32 ExtensionCount = 0;
	VkExtensionProperties *Properties = NULL;
	vkEnumerateDeviceExtensionProperties(Device, 0, &ExtensionCount, 0);
	if(ExtensionCount > 0)
	{
		Alloc(Properties, ExtensionCount);
		vkEnumerateDeviceExtensionProperties(Device, 0, &ExtensionCount, Properties);
	}

	for(int i = 0; i < ExtensionCount; i++)
	{
		if(strcmp(ExtensionString, Properties[i].extensionName) == 0)
		{
			supported = true;
			break;
		}
	}

	Free(Properties);

	return supported;
}

// @Incomplete
bool VK_DeviceIsSuitable(VkPhysicalDevice Device)
{
	bool suitable = true;
	return suitable;
}

VkShaderModule VK_CreateShaderModule(VkDevice Device, const char *path)
{
	fs_file File = FS_ReadEntireFile(path);
	VK_Assert(File.success == 1, "Could not shader file '%s'", path);

	VkShaderModuleCreateInfo CreateInfo = {0};
	VkShaderModule Shader;
	CreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
	CreateInfo.codeSize = File.size;
	CreateInfo.pCode = (const uint32_t *)File.data;

	VK_CHECK_RESULT(vkCreateShaderModule(Device, &CreateInfo, 0, &Shader));

	FS_FreeFile(&File);

	return Shader;
}


void VK_RecordCommandBuffer(
		VkCommandBuffer *CommandBuffer,
		u32 ImageIndex,
		VkRenderPass *RenderPass,
		VkFramebuffer *SwapChainFramebuffers,
		VkExtent2D SwapChainExtent,
		VkPipeline *GraphicsPipeline,
		VkPipelineLayout *PipelineLayout,
		float RotationDeg)
{
	float RotationMatrix[MATRIX_SIZE] = {0};
	RotationMatrix[0] = 1;
	RotationMatrix[3] = 1;

	// Rotate the identity matrix
	// This is row major notation
	// +cos, -sin
	// +sin, +cos
	float degrees = RotationDeg;
	float radians = Radians(degrees);

	// I am assuming column major memory layout in the shader for now
	// See shaders/tri.vert
	RotationMatrix[0] = cos(radians);
	RotationMatrix[1] = sin(radians);
	RotationMatrix[2] = -sin(radians);
	RotationMatrix[3]  = cos(radians);


	VkCommandBufferBeginInfo CommandBeginInfo = {0};
	CommandBeginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;

	// Begin command buffer
	VK_CHECK_RESULT(vkBeginCommandBuffer(*CommandBuffer, &CommandBeginInfo));

	VkRenderPassBeginInfo RenderBeginInfo = {0};
	RenderBeginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
	RenderBeginInfo.renderPass = *RenderPass;
	RenderBeginInfo.framebuffer = SwapChainFramebuffers[ImageIndex];
	RenderBeginInfo.renderArea.offset.x = 0;
	RenderBeginInfo.renderArea.offset.y = 0;
	RenderBeginInfo.renderArea.extent = SwapChainExtent;


	float cc = 0.0;
	VkClearValue ClearColor = {cc, cc, cc, 1};
	RenderBeginInfo.clearValueCount = 1;
	RenderBeginInfo.pClearValues = &ClearColor;

	vkCmdBeginRenderPass(*CommandBuffer, &RenderBeginInfo, VK_SUBPASS_CONTENTS_INLINE);

	vkCmdBindPipeline(*CommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, *GraphicsPipeline);
	{
		VkViewport Vp = {0};
		Vp.x = 0;
		Vp.y = 0;
		Vp.width = SwapChainExtent.width;
		Vp.height = SwapChainExtent.height;
		Vp.minDepth = 0;
		Vp.maxDepth = 0;
		vkCmdSetViewport(*CommandBuffer, 0, 1, &Vp);

		VkRect2D Scissor = {0};
		Scissor.extent = SwapChainExtent;
		vkCmdSetScissor(*CommandBuffer, 0, 1, &Scissor);


		vkCmdPushConstants(*CommandBuffer, *PipelineLayout, VK_SHADER_STAGE_VERTEX_BIT, 0, sizeof(RotationMatrix), RotationMatrix);

		vkCmdDraw(*CommandBuffer, 3, 1, 0, 0);

	}

	vkCmdEndRenderPass(*CommandBuffer);
	VK_CHECK_RESULT(vkEndCommandBuffer(*CommandBuffer));
}

int main()
{
	glfwInit();
	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

	// You can use GLFW_FALSE when debugging or writing the boilerplate for this code
	glfwWindowHint(GLFW_VISIBLE, GLFW_TRUE);

	GLFWwindow *Window = glfwCreateWindow(800, 600, "Hello Vulkan", 0, 0);
	glfwSwapInterval(1); // Vsync
	glfwSetKeyCallback(Window, CbKeys);
	glfwSetWindowCloseCallback(Window, CbWindowClose);

	int ExtensionCount = 0;
	const char *RuntimeExtensions[ARRAY_SIZE] = {0};
	const char **extensions = NULL;

	// Must live for the lifetime of the program. Validation layer identifers
	const char *ValidationLayerExt = VK_EXT_DEBUG_UTILS_EXTENSION_NAME;
	const char *ValidationLayerString = "VK_LAYER_KHRONOS_validation";
	extensions = glfwGetRequiredInstanceExtensions(&ExtensionCount);


	// Create Instance
	VkApplicationInfo AppInfo = {0};
	AppInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
	AppInfo.pApplicationName = "Hello, Vulkan";
	AppInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
	AppInfo.pEngineName = "No Engine";
	AppInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
	AppInfo.apiVersion = VK_API_VERSION_1_0;

	VkInstanceCreateInfo InstanceCreateInfo = {0};
	InstanceCreateInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
	InstanceCreateInfo.pApplicationInfo = &AppInfo;
	InstanceCreateInfo.enabledExtensionCount = ExtensionCount;
	InstanceCreateInfo.ppEnabledExtensionNames = extensions;
	InstanceCreateInfo.enabledLayerCount = 0;

	if(VK_CheckValidationLayerSupport(ValidationLayerString))
	{
		assert(ExtensionCount + 1 < ARRAY_SIZE);
		// Include validation layer extension along with GLFW's default extensions
		int i;
		for(i = 0; i < ExtensionCount; i++)
		{
			RuntimeExtensions[i] = extensions[i];
		}
		RuntimeExtensions[i] = ValidationLayerExt;
		InstanceCreateInfo.enabledExtensionCount = ExtensionCount + 1;
		InstanceCreateInfo.ppEnabledExtensionNames = RuntimeExtensions;
		InstanceCreateInfo.enabledLayerCount = 1;
		InstanceCreateInfo.ppEnabledLayerNames = &ValidationLayerString;

	}

	VkInstance Instance = {0};
	VK_CHECK_RESULT(vkCreateInstance(&InstanceCreateInfo, 0, &Instance));

	// Window surface
	// I bypass the platform specific stuff and use glfw to do this
	// The vulkan-tutorial.com page has an explanation for Win32
	// In the tutorial this is created much later, but we actually need this before any of the queue stuff...
	VkSurfaceKHR Surface;
	VK_CHECK_RESULT(glfwCreateWindowSurface(Instance, Window, 0, &Surface));


	// Pick physical device
	u32 DeviceCount = 0;
	VkPhysicalDevice PhysicalDevice = {0};
	vkEnumeratePhysicalDevices(Instance, &DeviceCount, 0);
	VK_Assert(DeviceCount != 0, "No devices found");
	VkPhysicalDevice *Devices;
	Alloc(Devices, DeviceCount);
	vkEnumeratePhysicalDevices(Instance, &DeviceCount, Devices);

	for(int i = 0; i < DeviceCount; i++)
	{
		VkPhysicalDevice Device = Devices[i];
		if(VK_DeviceIsSuitable(Device))
		{
			PhysicalDevice = Device;
		}
	}
	VK_Assert(PhysicalDevice != NULL, "Could not find a suitable device");

	// Queue families
	u32 QueueFamilyCount = 0;
	int QueueFamilyIndex = -1;
	int PresentFamilyIndex = -1;
	VkQueueFamilyProperties *QueueProperties;
	vkGetPhysicalDeviceQueueFamilyProperties(PhysicalDevice, &QueueFamilyCount, 0);
	VK_Assert(QueueFamilyCount != 0, "No physical device properties");
	Alloc(QueueProperties, QueueFamilyCount);
	vkGetPhysicalDeviceQueueFamilyProperties(PhysicalDevice, &QueueFamilyCount, QueueProperties);
	for(int i = 0; i < QueueFamilyCount; i++)
	{
		if(QueueProperties[i].queueFlags & VK_QUEUE_GRAPHICS_BIT)
		{
			QueueFamilyIndex = i;
		}
		VkBool32 PresentSupport = 0;
		vkGetPhysicalDeviceSurfaceSupportKHR(PhysicalDevice, i, Surface, &PresentSupport);
		if(PresentSupport)
		{
			PresentFamilyIndex = i;
		}
	}

	VK_Assert(QueueFamilyIndex != -1, "No queue family index");
	VK_Assert(PresentFamilyIndex != -1, "No present index");

	float QueuePriority = 1.0f;
	VkDeviceQueueCreateInfo QueueCreateInfos[2] = {0};
	u32 FamilySet[ArrayCount(QueueCreateInfos)] = {QueueFamilyIndex, PresentFamilyIndex};
	u32 UniqueFamilyCount = ArrayCount(QueueCreateInfos);
	// Apparently the QueueFamilyIndex and PresentFamilyIndex have to be unique??
	// If they're the same, we only add a single thing
	// The tutorial uses std::set, which naturally de-duplicates its elements
	if(QueueFamilyIndex == PresentFamilyIndex)
	{
		UniqueFamilyCount = 1;
	}
	for(int i = 0; i < UniqueFamilyCount; i++)
	{
		VkDeviceQueueCreateInfo QueueCreateInfo = {0};
		QueueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
		QueueCreateInfo.queueFamilyIndex = FamilySet[i];
		QueueCreateInfo.queueCount = 1;
		QueueCreateInfo.pQueuePriorities = &QueuePriority;

		QueueCreateInfos[i] = QueueCreateInfo;
	}


	// Create logical device
	const char *SwapExtensionString = VK_KHR_SWAPCHAIN_EXTENSION_NAME;
	VkDevice LogicalDevice = {0};
	VkPhysicalDeviceFeatures DeviceFeatures = {0};
	VkDeviceCreateInfo DeviceCreateInfo = {0};
	DeviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
	DeviceCreateInfo.pQueueCreateInfos = QueueCreateInfos;
	DeviceCreateInfo.queueCreateInfoCount = UniqueFamilyCount;
	DeviceCreateInfo.pEnabledFeatures = &DeviceFeatures;
	DeviceCreateInfo.enabledExtensionCount = 0;

	// Usage of a swap chain is an extension...
	if(VK_CheckDeviceExtensionSupport(PhysicalDevice, SwapExtensionString))
	{
		DeviceCreateInfo.enabledExtensionCount = 1;
		DeviceCreateInfo.ppEnabledExtensionNames = &SwapExtensionString;
	}

	// This struct also needs to know about validation layers
	// Apparently not needed for newer Vulkan implementations, but just to be safe...
	if(VK_CheckValidationLayerSupport(ValidationLayerString))
	{
		DeviceCreateInfo.enabledLayerCount = 1;
		DeviceCreateInfo.ppEnabledLayerNames = &ValidationLayerString;
	}

	VK_CHECK_RESULT(vkCreateDevice(PhysicalDevice, &DeviceCreateInfo, 0, &LogicalDevice));


	// Create the present queue
	VkQueue PresentQueue = {0};
	VkQueue GraphicsQueue = {0};
	vkGetDeviceQueue(LogicalDevice, PresentFamilyIndex, 0, &PresentQueue);
	vkGetDeviceQueue(LogicalDevice, QueueFamilyIndex, 0, &GraphicsQueue);


	// Query the swap chain
	vk_swap_chain_desc SwapChainDesc = VK_QuerySwapChainDesc(PhysicalDevice, Surface);
	VkSurfaceFormatKHR SurfaceFormat = VK_ChooseSwapSurfaceFormat(&SwapChainDesc);
	VkPresentModeKHR PresentMode = VK_ChooseSwapPresentMode(&SwapChainDesc);
	VkExtent2D Extent = VK_ChooseSwapExtent(&SwapChainDesc);

	// Try to double buffer by default
	u32 ImageCount = SwapChainDesc.Caps.minImageCount + 1;
	u32 MaxCount = SwapChainDesc.Caps.maxImageCount;
	if(MaxCount > 0 && ImageCount > MaxCount) ImageCount = MaxCount;

	// Create the swap chain
	VkSwapchainCreateInfoKHR SwapChainCreateInfo = {0};
	SwapChainCreateInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
	SwapChainCreateInfo.surface = Surface;
	SwapChainCreateInfo.minImageCount = ImageCount;
	SwapChainCreateInfo.imageFormat = SurfaceFormat.format;
	SwapChainCreateInfo.imageColorSpace = SurfaceFormat.colorSpace;
	SwapChainCreateInfo.imageExtent = Extent;
	SwapChainCreateInfo.imageArrayLayers = 1;
	SwapChainCreateInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
	SwapChainCreateInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;

	// Some magic in case the queue families differ
	if(PresentFamilyIndex != QueueFamilyIndex)
	{
		SwapChainCreateInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
		SwapChainCreateInfo.queueFamilyIndexCount = 2;
		SwapChainCreateInfo.pQueueFamilyIndices = FamilySet;
	}

	SwapChainCreateInfo.preTransform = SwapChainDesc.Caps.currentTransform;
	SwapChainCreateInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	SwapChainCreateInfo.presentMode = PresentMode;
	SwapChainCreateInfo.clipped = VK_TRUE;
	SwapChainCreateInfo.oldSwapchain = VK_NULL_HANDLE;

	VkSwapchainKHR SwapChain = {0};
	VK_CHECK_RESULT(vkCreateSwapchainKHR(LogicalDevice, &SwapChainCreateInfo, 0, &SwapChain));


	VkImage *SwapChainImages = NULL;
	vkGetSwapchainImagesKHR(LogicalDevice, SwapChain, &ImageCount, 0);
	VK_Assert(ImageCount > 0, "No images in the swap chain");
	Alloc(SwapChainImages, ImageCount);
	vkGetSwapchainImagesKHR(LogicalDevice, SwapChain, &ImageCount, SwapChainImages);

	// The tutorial suggests that the followed variables be cached
	VkFormat SwapChainFormat = SurfaceFormat.format;
	VkExtent2D SwapChainExtent = Extent;



	// Image views
	VkImageView *SwapChainImageViews;
	Alloc(SwapChainImageViews, ImageCount);
	for(int i = 0; i < ImageCount; i++)
	{
		VkImageViewCreateInfo CreateInfo = {0};
		CreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
		CreateInfo.image = SwapChainImages[i];
		CreateInfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
		CreateInfo.format = SwapChainFormat;

		// Nice for color masks I guess?
		// Could be a potentially quick way to convert from rgba to bgra
		CreateInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
		CreateInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
		CreateInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
		CreateInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;

		CreateInfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
		CreateInfo.subresourceRange.baseMipLevel = 0;
		CreateInfo.subresourceRange.levelCount = 1;
		CreateInfo.subresourceRange.baseArrayLayer = 0;
		CreateInfo.subresourceRange.layerCount = 1;

		VK_CHECK_RESULT(vkCreateImageView(LogicalDevice, &CreateInfo, 0, &SwapChainImageViews[i]));
	}


	// Create graphics pipeline
	VkShaderModule VertShader = VK_CreateShaderModule(LogicalDevice,  "shaders/bin/tri.vert.spv");
	VkShaderModule FragShader = VK_CreateShaderModule(LogicalDevice,  "shaders/bin/tri.frag.spv");

	VkPipelineShaderStageCreateInfo VertShaderInfo = {0};
	VertShaderInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	VertShaderInfo.stage= VK_SHADER_STAGE_VERTEX_BIT;
	VertShaderInfo.module = VertShader;
	VertShaderInfo.pName = "main";

	VkPipelineShaderStageCreateInfo FragShaderInfo = {0};
	FragShaderInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
	FragShaderInfo.stage= VK_SHADER_STAGE_FRAGMENT_BIT;
	FragShaderInfo.module = FragShader;
	FragShaderInfo.pName = "main";

	VkPipelineShaderStageCreateInfo ShaderStages[2] =
	{
		VertShaderInfo,
		FragShaderInfo
	};

	// Dynamic states
	// These states can be changed during runtime without re-creating them each frame
	VkDynamicState DynamicStates[2] =
	{
		VK_DYNAMIC_STATE_VIEWPORT,
		VK_DYNAMIC_STATE_SCISSOR
	};

	VkPipelineDynamicStateCreateInfo DynamicInfo = {0};
	DynamicInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
	DynamicInfo.dynamicStateCount = ArrayCount(DynamicStates);
	DynamicInfo.pDynamicStates = DynamicStates;


	// Vertex assembly
	// The tutorial has it 0'ed out by default, except the type.
	VkPipelineVertexInputStateCreateInfo VertexInfo = {0};
	VertexInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;


	// Assembly state
	// Describes the topology of the primitives
	VkPipelineInputAssemblyStateCreateInfo InputInfo = {0};
	InputInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
	InputInfo.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
	InputInfo.primitiveRestartEnable = VK_FALSE;


	// Viewport
	VkViewport Viewport = {0};
	Viewport.x = 0;
	Viewport.y = 0;
	Viewport.width = SwapChainExtent.width;
	Viewport.height = SwapChainExtent.height;
	Viewport.minDepth = 0;
	Viewport.maxDepth = 1;


	// Scissor rectangle
	VkRect2D ScissorRect = {0};
	ScissorRect.offset.x = 0;
	ScissorRect.offset.y = 0;
	ScissorRect.extent = SwapChainExtent;


	// Viewport state
	VkPipelineViewportStateCreateInfo ViewportInfo = {0};
	ViewportInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
	ViewportInfo.viewportCount = 1;
	ViewportInfo.pViewports = &Viewport;
	ViewportInfo.scissorCount = 1;
	ViewportInfo.pScissors = &ScissorRect;


	// Rasterizer
	VkPipelineRasterizationStateCreateInfo RasterizerInfo = {0};
	RasterizerInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
	RasterizerInfo.depthClampEnable = VK_FALSE;
	RasterizerInfo.polygonMode = VK_POLYGON_MODE_FILL;
	RasterizerInfo.lineWidth = 1;
	// @Incomplete
	// Try to go without culling for now
	// RasterizerInfo.cullMode = VK_CULL_MODE_BACK_BIT;
	// RasterizerInfo.frontFace = VK_FRONT_FACE_CLOCKWISE;
	RasterizerInfo.depthBiasEnable = VK_FALSE;


	// Multisampling
	VkPipelineMultisampleStateCreateInfo MultisampleInfo = {0};
	MultisampleInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
	MultisampleInfo.sampleShadingEnable = VK_FALSE;
	MultisampleInfo.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
	MultisampleInfo.minSampleShading = 1.0f;

	// No depth buffer yet

	// Color state
	// This is like glBlendFunc in struct form
	// It has some more parameters that I'm not setting here yet
	VkPipelineColorBlendAttachmentState ColorBlendAttachment = {0};
	ColorBlendAttachment.colorWriteMask =
		  VK_COLOR_COMPONENT_R_BIT
		| VK_COLOR_COMPONENT_G_BIT
		| VK_COLOR_COMPONENT_B_BIT
		| VK_COLOR_COMPONENT_A_BIT
		;

	ColorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_ONE;
	ColorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ONE;

	// Color blend info
	// This is distinct from ColorBlendAttachment
	VkPipelineColorBlendStateCreateInfo ColorBlendInfo = {0};
	ColorBlendInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
	ColorBlendInfo.logicOpEnable = VK_FALSE;
	ColorBlendInfo.attachmentCount = 1;
	ColorBlendInfo.pAttachments = &ColorBlendAttachment;


	// Pipeline layout
	VkPipelineLayout PipelineLayout;
	VkPipelineLayoutCreateInfo PipelineLayoutInfo = {0};
	PipelineLayoutInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;

	// This part wasn't in the original tutorial. I'm adding support for push constants
	float RotationMatrix[MATRIX_SIZE];
	VkPushConstantRange PushConstantRange = {0};
	PushConstantRange.offset = 0;
	PushConstantRange.size = sizeof(RotationMatrix);
	PushConstantRange.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;

	PipelineLayoutInfo.pPushConstantRanges = &PushConstantRange;
	PipelineLayoutInfo.pushConstantRangeCount = 1;



	VK_CHECK_RESULT(vkCreatePipelineLayout(LogicalDevice, &PipelineLayoutInfo, 0, &PipelineLayout));



// https://vulkan-tutorial.com/en/Drawing_a_triangle/Graphics_pipeline_basics/Render_passes
	// Render pass
	// This is basically the framebuffer we're drawing to
	VkAttachmentDescription ColorAttachment ={0};
	ColorAttachment.format = SwapChainFormat;
	ColorAttachment.samples = VK_SAMPLE_COUNT_1_BIT;
	ColorAttachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
	ColorAttachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
	ColorAttachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
	ColorAttachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

	VkAttachmentReference ColorAttachmentRef = {0};
	ColorAttachmentRef.attachment = 0;
	ColorAttachmentRef.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;


	// Subpass
	VkSubpassDescription Subpass = {0};
	Subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
	Subpass.colorAttachmentCount = 1;
	Subpass.pColorAttachments = &ColorAttachmentRef;

	// Render pass (actual struct)
	VkRenderPassCreateInfo RenderPassInfo = {0};
	RenderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
	RenderPassInfo.attachmentCount = 1;
	RenderPassInfo.pAttachments = &ColorAttachment;
	RenderPassInfo.subpassCount = 1;
	RenderPassInfo.pSubpasses = &Subpass;

	VkRenderPass RenderPass;
	VK_CHECK_RESULT(vkCreateRenderPass(LogicalDevice, &RenderPassInfo, 0, &RenderPass));


	// Graphics pipeline
	// Brings everything together
	VkGraphicsPipelineCreateInfo GraphicsInfo = {0};
	GraphicsInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
	GraphicsInfo.stageCount = 2;
	GraphicsInfo.pStages = ShaderStages;
	GraphicsInfo.pVertexInputState = &VertexInfo;
	GraphicsInfo.pInputAssemblyState = &InputInfo;
	GraphicsInfo.pViewportState = &ViewportInfo;
	GraphicsInfo.pRasterizationState = &RasterizerInfo;
	GraphicsInfo.pMultisampleState = &MultisampleInfo;
	GraphicsInfo.pDepthStencilState = 0;
	GraphicsInfo.pColorBlendState = &ColorBlendInfo;
	GraphicsInfo.pDynamicState = &DynamicInfo;

	GraphicsInfo.layout = PipelineLayout;
	GraphicsInfo.renderPass = RenderPass;
	GraphicsInfo.subpass = 0;
	GraphicsInfo.basePipelineIndex = -1;


	VkPipeline GraphicsPipeline;
	VK_CHECK_RESULT(vkCreateGraphicsPipelines(LogicalDevice, 0, 1, &GraphicsInfo, 0, &GraphicsPipeline));



	// Framebuffer creation
	VkFramebuffer *SwapChainFramebuffers;
	Alloc(SwapChainFramebuffers, ImageCount);

	for(int i = 0; i < ImageCount; i++)
	{
		VkFramebufferCreateInfo FramebufferInfo = {0};
		FramebufferInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
		FramebufferInfo.renderPass = RenderPass;
		FramebufferInfo.attachmentCount = 1;
		FramebufferInfo.pAttachments = &SwapChainImageViews[i];
		FramebufferInfo.width = SwapChainExtent.width;
		FramebufferInfo.height = SwapChainExtent.height;
		FramebufferInfo.layers = 1;

		VK_CHECK_RESULT(vkCreateFramebuffer(LogicalDevice, &FramebufferInfo, 0, &SwapChainFramebuffers[i]));
	}



	// Command pool
	VkCommandPoolCreateInfo CommandPoolInfo = {0};
	CommandPoolInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
	CommandPoolInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
	CommandPoolInfo.queueFamilyIndex = QueueFamilyIndex;

	VkCommandPool CommandPool;
	VK_CHECK_RESULT(vkCreateCommandPool(LogicalDevice, &CommandPoolInfo, 0, &CommandPool));



	// Command buffer
	VkCommandBufferAllocateInfo CommandBufferInfo = {0};
	CommandBufferInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
	CommandBufferInfo.commandPool = CommandPool;
	CommandBufferInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
	CommandBufferInfo.commandBufferCount = 1;

	VkCommandBuffer CommandBuffer;
	VK_CHECK_RESULT(vkAllocateCommandBuffers(LogicalDevice, &CommandBufferInfo, &CommandBuffer));


	// Recording a command buffer
	// This is a seperate function in the tutorial, and is implemented in VK_RecordCommandBuffer



	// Break here for now
	// https://vulkan-tutorial.com/en/Drawing_a_triangle/Drawing/Rendering_and_presentation
	// Locks
	VkSemaphore ImageAvailableSem;
	VkSemaphore RenderFinishedSem;
	VkFence InFlightFence;

	VkFenceCreateInfo FenceInfo = {0};
	FenceInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
	FenceInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

	VkSemaphoreCreateInfo SemInfo = {0};
	SemInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

	VK_CHECK_RESULT(vkCreateSemaphore(LogicalDevice, &SemInfo, 0, &ImageAvailableSem));

	VK_CHECK_RESULT(vkCreateSemaphore(LogicalDevice, &SemInfo, 0, &RenderFinishedSem));

	VK_CHECK_RESULT(vkCreateFence(LogicalDevice, &FenceInfo, 0, &InFlightFence));


	// I didn't know when we would last use this, so it's getting freed right before the main loop
	VK_FreeSwapChainDesc(&SwapChainDesc);

	float Rotation = 0;
	while(Running)
	{
		double t1 = glfwGetTime();

		// Draw frame
		vkWaitForFences(LogicalDevice, 1, &InFlightFence, VK_TRUE, UINT64_MAX);
		vkResetFences(LogicalDevice, 1, &InFlightFence);

		u32 ImageIndex = 0;
		vkAcquireNextImageKHR(LogicalDevice, SwapChain, UINT64_MAX, ImageAvailableSem, 0, &ImageIndex);
		vkResetCommandBuffer(CommandBuffer, 0);

		VK_RecordCommandBuffer(&CommandBuffer, ImageIndex, &RenderPass, SwapChainFramebuffers, SwapChainExtent, &GraphicsPipeline, &PipelineLayout, Rotation);
		Rotation++;

		VkSubmitInfo SubmitInfo = {0};
		SubmitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
		VkSemaphore WaitSemaphores[1] = {ImageAvailableSem};
		VkPipelineStageFlags WaitStages[1] = {VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT};

		SubmitInfo.waitSemaphoreCount = 1;
		SubmitInfo.pWaitSemaphores = WaitSemaphores;
		SubmitInfo.pWaitDstStageMask = WaitStages;
		SubmitInfo.commandBufferCount = 1;
		SubmitInfo.pCommandBuffers = &CommandBuffer;

		VkSemaphore SignalSemaphores[1] = {RenderFinishedSem};
		SubmitInfo.signalSemaphoreCount = 1;
		SubmitInfo.pSignalSemaphores = SignalSemaphores;

		VK_CHECK_RESULT(vkQueueSubmit(GraphicsQueue, 1, &SubmitInfo, InFlightFence));


		VkSubpassDependency Dependency = {0};
		Dependency.srcSubpass = VK_SUBPASS_EXTERNAL;
		Dependency.dstSubpass = 0;
		Dependency.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		Dependency.srcAccessMask = 0;

		Dependency.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;
		Dependency.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

		RenderPassInfo.dependencyCount = 1;
		RenderPassInfo.pDependencies = &Dependency;


		// Present
		VkPresentInfoKHR PresentInfo = {0};
		PresentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
		PresentInfo.waitSemaphoreCount = 1;
		PresentInfo.pWaitSemaphores = SignalSemaphores;

		VkSwapchainKHR SwapChains[1] = {SwapChain};
		PresentInfo.swapchainCount = 1;
		PresentInfo.pSwapchains = SwapChains;
		PresentInfo.pImageIndices = &ImageIndex;
		PresentInfo.pResults = 0;

		vkQueuePresentKHR(PresentQueue, &PresentInfo);


		glfwSwapBuffers(Window);
		glfwPollEvents();

		double t2 = glfwGetTime();

		// Uncomment these lines to confirm that you are running at the monitor's refresh rate
		// double ms  = (t2 - t1) * 1000.0;
		// printf("Frame time: %.2f ms\n", ms);
	}

	glfwTerminate();

	return 0;
}
