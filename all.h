#ifndef _ALL_H_
#define _ALL_H_

#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include <sys/stat.h>
#include <errno.h>
#include <math.h>
#include <limits.h>


#include "types.h"


#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>


#endif /* _ALL_H_ */
