# Hello Vulkan

```
All of the steps required to display a triangle using the Vulkan graphics API.

Not quite as simple as it used to be :)
unsigned char far *VRAM = 0xA0000000;
asm {
	mov ax, 0x13
	int 0x10
}
VRAM[0] = 1;


Building:
make
./vulkan-demo


Dependencies:
GLFW 3
Vulkan 1.2
glslc (optional, pre-compiled shaders are included)
```

![thumbnail](./thumbnail.png)


## References
I really couldn't have done it without these explanations

[Vulkan Tutorial](https://vulkan-tutorial.com)

[Push Constants](https://vkguide.dev/docs/chapter-3/push_constants/)
