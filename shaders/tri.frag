#version 450

layout (location = 0) in vec3 vf_Color;
layout (location = 0) out vec4 FragColor;

void main()
{
	FragColor = vec4(vf_Color, 1);
}
