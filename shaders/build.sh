#!/bin/sh

mkdir -p bin


for f in *.vert; do
	printf "Compiling $f\n"
	glslc "$f" -o bin/"$f".spv
done

for f in *.frag; do
	printf "Compiling $f\n"
	glslc "$f" -o bin/"$f".spv
done
