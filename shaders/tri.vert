#version 450

layout(location = 0) out vec3 vf_Color;

// Vulkan's coordinate system differs from OpenGL's
// Vulkan: Origin is top left
// OpenGL: Origin is bottom left

// Vulkan
/*
|-----------------|
| -1, -1 | +1, -1 |
|--------|--------|
| -1, +1 | +1, +1 |
|-----------------|
*/


// OpenGL
/*
|-----------------|
| -1, +1 | +1, +1 |
|--------|--------|
| -1, -1 | +1, -1 |
|-----------------|
*/

// Therefore the triangle coordinates will differ from the OpenGL hello triangle

vec2 tris[] =
{
	vec2(-0.5, 0.5),
	vec2(0.0, -0.5),
	vec2(0.5, 0.5)
};

vec3 colors[] =
{
	vec3(1, 0, 0),
	vec3(0, 1,  0),
	vec3(0, 0, 1)
};

layout(push_constant) uniform constants
{
	 mat2 rotation;
} PushConstants;

void main()
{
	// In glslc, gl_VertexID is now gl_VertexIndex
	int idx = gl_VertexIndex;
	mat2 rotation = PushConstants.rotation;
	vec2 pos = rotation * tris[idx];
	gl_Position = vec4(pos, 0, 1);

	vf_Color = colors[idx];
}
